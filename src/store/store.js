import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    rows: [
      {
        id: '1fila',
        columns: [
        {id: 'columna1', content: '<div> columna1 </div>'},
        {id: 'columna1', content: '<div> columna1 </div>'},
        {id: 'columna1', content: '<div> columna1 </div>'}
        ]
      },
      { id: '4fila',
        columns: [
         {id: 'columna1', content: '<div> columna1 </div>'}
        ]
      }
    ]
  },
  mutations: {
    aumentar: (state) => state.cantidad++,
    reducir: (state) => state.cantidad--,
    AddRow (state) {
      state.rows.push(
        {
          id: 'fila-' + Math.floor(Math.random() * (500 - 0)),
          columns: [
            {
              id: 'columna1',
              content: '<div> ' + Math.floor(Math.random() * (500 - 0)) + ' </div>'
            }
          ]
        }
      )
    },
    DeleteRow (state, index) {
      state.rows.splice(index, 1)
    }
  }
})
